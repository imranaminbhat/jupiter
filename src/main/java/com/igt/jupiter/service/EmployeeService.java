package com.igt.jupiter.service;

import java.util.List;

import com.igt.jupiter.model.Employee;

public interface EmployeeService {
	
	public Employee save(Employee employee);

	public Employee update(Employee employee);

	public void delete(String employeId);

	public Employee findById(String id);

	public List<Employee> findAll();
}
