package com.igt.jupiter.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.igt.jupiter.model.Employee;
import com.igt.jupiter.repository.EmployeeRepository;
import com.igt.jupiter.service.EmployeeService;

@Transactional
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository empRepo;
	
	@Override
	public Employee save(Employee employee) {
		return empRepo.save(employee);
	}

	@Override
	public Employee update(Employee employee) {
		return empRepo.save(employee);
	}
	
	@Override
	public void delete(String employeId) {
		empRepo.delete(employeId);
	}
	
	@Override
	public Employee findById(String id) {
		return empRepo.findOne(id);
	}
	
	@Override
	public List<Employee> findAll() {
		return empRepo.findAll();
	}
}
