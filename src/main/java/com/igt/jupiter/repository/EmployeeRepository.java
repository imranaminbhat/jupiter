package com.igt.jupiter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.igt.jupiter.model.Employee;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {

}
