package com.igt.jupiter.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories(basePackages = "com.igt.jupiter.repository")
public class MongoDBConfig extends AbstractMongoConfiguration {

	/**
	*Below literals should ideally be loaded from property file but due to 
	*shortage of time, these are hard coded here.
	 */
	
	@Override
	protected String getDatabaseName() {
		return "igt_db";
	}

	@Override
	public Mongo mongo() throws Exception {
		return  new MongoClient("127.0.0.1", 27017);
	}
	
	@Override
    protected String getMappingBasePackage() {
        return "com.igt";
    }

}
