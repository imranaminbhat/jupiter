package com.igt.jupiter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.igt.jupiter.model.Employee;
import com.igt.jupiter.service.EmployeeService;

@RestController
@RequestMapping("/api/employes")
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@RequestMapping(value="/createEmploye", method=RequestMethod.POST)
	public Employee save(@RequestBody Employee employee) {
		return employeeService.save(employee);
	}
	
	@RequestMapping(value="/updateEmploye", method=RequestMethod.POST)
	public Employee update(@RequestBody Employee employee) {
			return employeeService.update(employee);
	}
	
	@RequestMapping(value="/deleteEmploye/{employeId}", method=RequestMethod.GET)
	public void delete(@PathVariable String employeId) {
			employeeService.delete(employeId);
	}
	
	@RequestMapping(value="/getEmploye/{id}", method=RequestMethod.GET)
	public Employee findById(@PathVariable String id) {
		return employeeService.findById(id);
			
	}
	
	@RequestMapping(value="/allEmployes", method=RequestMethod.GET)
	public List<Employee> findAll() {
		return employeeService.findAll();
			
	}
}
